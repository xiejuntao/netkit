package xjt.netkit;

import java.util.Iterator;

import xjt.netkit.exception.ActionException;
import xjt.netkit.message.Message;

public class ActionChain {
	private Iterator<ActionFilter> it;

	public void setIterator(Iterator<ActionFilter> it) {
		this.it = it;
	}
	public void doChain(Action action, Message message) throws ActionException {
		if (this.it.hasNext()) {
			this.it.next().doFilter(action, message, this);
			return;
		}
		action.execute(message);
	}
}