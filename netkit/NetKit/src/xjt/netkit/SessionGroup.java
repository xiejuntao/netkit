package xjt.netkit;

import io.netty.channel.Channel;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelFutureListener;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

public class SessionGroup {
	private final ConcurrentMap<Serializable, Session> sessionMap = new ConcurrentHashMap<Serializable, Session>();

	private final ChannelFutureListener remover = new ChannelFutureListener() {
		@Override
		public void operationComplete(ChannelFuture future) throws Exception {
			int sessionId = future.channel().hashCode();
			List<Serializable> keys = SessionGroup.this.findKeysBySessionId(sessionId);
			SessionGroup.this.remove(keys);
		}
	};

	public synchronized boolean add(Serializable key, Session s) {
		if ((s instanceof Session)) {
			Session session = s;
			Channel channel = session.getChannel();

			boolean added = this.sessionMap.putIfAbsent(key, s) == null;
			if (added) {
				channel.closeFuture().addListener(this.remover);
			}
			return added;
		}
		return false;
	}

	public synchronized void close() {
		for (Map.Entry<Serializable,Session> entry : this.sessionMap.entrySet()) {
			Session s = entry.getValue();
			s.close();
		}
		this.sessionMap.clear();
	}

	public boolean isEmpty() {
		return this.sessionMap.isEmpty();
	}

	public synchronized int remove(Session session) {
		if (session == null) {
			return 0;
		}
		List<Serializable> keys = findKeysBySessionId(session.getSessionId());
		int count = remove(keys);
		return count;
	}

	public synchronized Session remove(Serializable key) {
		Session s = this.sessionMap.remove(key);
		if (s == null) {
			return null;
		}
		s.getChannel().closeFuture().removeListener(this.remover);
		return s;
	}

	public synchronized int remove(Collection<Serializable> keys) {
		int count = 0;
		for (Serializable key : keys) {
			Session s = remove(key);
			if (s != null) {
				count++;
			}
		}
		return count;
	}

	public Session get(Serializable key) {
		return this.sessionMap.get(key);
	}

	public List<Serializable> findKeysBySessionId(int sessionId) {
		List<Serializable> keys = new ArrayList<Serializable>();
		for (Map.Entry<Serializable,Session> entry : this.sessionMap.entrySet()) {
			Session s = entry.getValue();
			if (s.getSessionId() == sessionId) {
				keys.add(entry.getKey());
			}
		}
		return keys;
	}

	public int size() {
		return this.sessionMap.size();
	}

	public Collection<Session> sessions() {
		return this.sessionMap.values();
	}

	public Collection<Serializable> keys() {
		return this.sessionMap.keySet();
	}
}