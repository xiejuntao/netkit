package xjt.netkit;

import java.util.List;

import org.apache.log4j.Logger;

import xjt.netkit.message.Message;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ByteToMessageDecoder;

public class NetkitProtocolDecoder extends ByteToMessageDecoder{
	Logger logger = Logger.getLogger(NetkitProtocolDecoder.class);
	@Override
	protected void decode(ChannelHandlerContext ctx, ByteBuf source,
			List<Object> out) throws Exception {
		if (source.readableBytes() < 8) {
			return;
		}
		source.markReaderIndex();
		try {
			int messageId = source.readInt();
			int contentLen = source.readInt();
			if(source.readableBytes() < contentLen){
				source.resetReaderIndex();
				return;
			}
			byte[] data = new byte[contentLen];
			source.readBytes(data);
			out.add(new Message(messageId, data));
		}catch (Exception e) {
			source.resetReaderIndex();
		}
	}
}
