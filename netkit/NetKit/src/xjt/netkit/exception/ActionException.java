package xjt.netkit.exception;

public class ActionException extends Exception {
	private static final long serialVersionUID = -2779165537258740936L;

	public ActionException() {
	}

	public ActionException(String message) {
		super(message);
	}

	public ActionException(Throwable e) {
		super(e);
	}

	public ActionException(String message, Throwable e) {
		super(message, e);
	}
}