package xjt.netkit;

import xjt.netkit.message.Message;

public interface MessageSendable {
	public Future sendMessage(Message paramMessage);
}
