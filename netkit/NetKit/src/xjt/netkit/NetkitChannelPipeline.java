package xjt.netkit;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;

public class NetkitChannelPipeline extends
		ChannelInitializer<SocketChannel> {
	public static final String EncodeHandlerName = "encoder";
	public static final String DecodeHandlerName = "decoder";
	public static final String MessageHandlerName = "handler";
	private final NetkitContext context;
	public NetkitChannelPipeline(NetkitContext context) {
		super();
		this.context = context;
	}
	@Override
	protected void initChannel(SocketChannel socketChannel) throws Exception {
		ChannelPipeline pipeline = socketChannel.pipeline();
		pipeline.addLast(DecodeHandlerName,new NetkitProtocolDecoder());
		pipeline.addLast(EncodeHandlerName,new NetkitProtocolEncoder());
		pipeline.addLast(MessageHandlerName,new NetkitChannelHandler(this.context));
	}

}
