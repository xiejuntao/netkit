package xjt.netkit.event;

import java.util.EventListener;

import xjt.netkit.Session;

public interface NetkitSessionListener extends EventListener {
	public void onEvent(Session paramSession);
}
