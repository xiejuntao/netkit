package xjt.netkit.event;

import java.util.EventListener;

import xjt.netkit.message.Message;

public interface NetkitChannelEventListener extends EventListener {
	public void onConnected(ChannelEvent paramChannelEvent);
	public void onDisconnected(ChannelEvent paramChannelEvent);

	public void onExceptionCaught(ChannelEvent paramChannelEvent,
			Throwable paramThrowable);
	public void onMessageReceived(ChannelEvent paramChannelEvent,
			Message paramMessage);
}
