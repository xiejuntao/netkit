package xjt.netkit.event;

import io.netty.channel.Channel;

import java.util.EventObject;

import xjt.netkit.NetkitContext;

public class ChannelEvent extends EventObject {
	private static final long serialVersionUID = 2552115504371760252L;
	private NetkitContext serverContext;
	private Channel channel;

	public ChannelEvent(NetkitContext serverContext, Channel channel) {
		super(channel);
		this.serverContext = serverContext;
		this.channel = channel;
	}

	public Channel getChannel() {
		return this.channel;
	}

	public int getChannelID() {
		return getChannel().hashCode();
	}

	public NetkitContext getServerContext() {
		return this.serverContext;
	}
}