package xjt.netkit.http;

import xjt.netkit.NetkitContext;
import xjt.netkit.config.Config;
import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
/**
 * http监控服务
 * */
public class HttpServer {
	protected static HttpServer httpServer;
	protected NetkitContext context;
	public HttpServer(NetkitContext context){
		this.context = context;
	}
	public void start(){
		try {
			EventLoopGroup bossGroup = new NioEventLoopGroup(1);
			EventLoopGroup workerGroup = new NioEventLoopGroup();
			ServerBootstrap b = new ServerBootstrap();
			b.option(ChannelOption.SO_BACKLOG, 1024);
			b.group(bossGroup, workerGroup)
					.channel(NioServerSocketChannel.class)
					.handler(new LoggingHandler(LogLevel.INFO))
					.childHandler(new HttpChannelInitializer(context));
			b.bind(Config.HTTP_ADDRESS,Config.HTTP_PORT).sync();
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
}
