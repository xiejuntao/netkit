package xjt.netkit.http;

import xjt.netkit.NetkitContext;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.http.HttpServerCodec;

public class HttpChannelInitializer extends ChannelInitializer<SocketChannel> {
	protected NetkitContext context;
	public HttpChannelInitializer(NetkitContext context) {
		super();
		this.context = context;
	}
	@Override
	protected void initChannel(SocketChannel ch) throws Exception {
        ChannelPipeline pipeline = ch.pipeline();
        pipeline.addLast(new HttpServerCodec());
        pipeline.addLast(new HttpDataHandler(context));
	}

}
