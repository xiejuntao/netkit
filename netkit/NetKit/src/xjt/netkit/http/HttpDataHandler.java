package xjt.netkit.http;

import java.net.URI;

import org.apache.log4j.Logger;

import xjt.netkit.NetkitContext;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.handler.codec.http.HttpObject;
import io.netty.handler.codec.http.HttpRequest;
import io.netty.handler.codec.http.multipart.DefaultHttpDataFactory;
import io.netty.handler.codec.http.multipart.HttpDataFactory;
import io.netty.handler.codec.http.multipart.HttpPostRequestDecoder;

public class HttpDataHandler extends SimpleChannelInboundHandler<HttpObject> {
	protected Logger logger = Logger.getLogger(HttpDataHandler.class);
	protected NetkitContext context;
	protected HttpDataFactory factory = new DefaultHttpDataFactory(
			DefaultHttpDataFactory.MINSIZE);
	protected HttpPostRequestDecoder decoder;
	protected HttpWrapper httpWrapper = null;
	protected HttpRequest httpRequest = null;
	public HttpDataHandler(NetkitContext context) {
		super();
		this.context = context;
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, HttpObject msg)
			throws Exception {
		if (msg instanceof HttpRequest){
			HttpRequest request = this.httpRequest = (HttpRequest) msg;
			httpWrapper = new HttpWrapper(ctx, request);
			String requestPath = new URI(request.getUri()).getPath();
			logger.info("requestPath=" + requestPath);
			if("/".equals(requestPath)){
				httpWrapper.write("当前客户端连接数:"+context.getConnectionNum()+"。");
			}
		}
		
	}

}
