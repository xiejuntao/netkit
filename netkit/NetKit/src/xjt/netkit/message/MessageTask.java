package xjt.netkit.message;

import xjt.netkit.MessageDispatcher;
import xjt.netkit.Session;
/**
 * 消息处理线程
 * */
public class MessageTask implements Runnable {
	private MessageDispatcher dispatcher;
	private Session session;
	private Message message;

	private MessageTask(MessageDispatcher dispatcher, Session session,
			Message msg) {
		this.dispatcher = dispatcher;
		this.session = session;
		this.message = msg;
	}

	@Override
	public void run() {
		execute();
	}

	public void execute() {
		this.dispatcher.dispatch(this.session.getContext(), this.session,
				this.message);
	}

	public static MessageTask newInstance(MessageDispatcher dispatcher,
			Session session, Message msg) {
		return new MessageTask(dispatcher, session, msg);
	}
}
