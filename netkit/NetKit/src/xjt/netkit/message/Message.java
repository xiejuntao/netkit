package xjt.netkit.message;
/**
 * 传输的消息，包括消息Id和消息内容
 * */
public class Message {
	public static final int HEADER_SIZE = 8;
	public static final int MessageID_Server_Max_Connections = 0;
	private int messageId;
	private byte[] messageContent;

	public Message() {
	}

	public Message(int messageId) {
		this.messageId = messageId;
	}

	public Message(int messageId, byte[] messageContent) {
		this.messageId = messageId;
		this.messageContent = messageContent;
	}

	public int getMessageId() {
		return this.messageId;
	}

	public void setMessageId(int messageId) {
		this.messageId = messageId;
	}

	public byte[] getMessageContent() {
		return this.messageContent;
	}

	public void setMessageContent(byte[] messageContent) {
		this.messageContent = messageContent;
	}

	public String getMessageContentAsString() {
		if (this.messageContent == null) {
			return null;
		}
		return new String(this.messageContent);
	}

	public int getPacketSize() {
		if (this.messageContent == null) {
			return 8;
		}
		return 8 + this.messageContent.length;
	}

	public int getContentLength() {
		if (this.messageContent == null) {
			return 0;
		}

		return this.messageContent.length;
	}
}