package xjt.netkit.message;

import org.apache.commons.lang3.StringUtils;


public class MessageUtil {
	public static Message getMessage(int messageId, String json) {
		if(StringUtils.isBlank(json)){
			return null;
		}
		Message message = new Message(messageId);
		message.setMessageContent(json.getBytes());
		return message;
	}
}
