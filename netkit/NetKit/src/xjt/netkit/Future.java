package xjt.netkit;

import java.util.concurrent.TimeUnit;

import io.netty.channel.ChannelFuture;
import io.netty.util.concurrent.FutureListener;

public class Future {
	private ChannelFuture channelFuture;
	private Session session;

	public Future(Session session, ChannelFuture channelFuture) {
		this.session = session;
		this.channelFuture = channelFuture;
	}

	public void addFutureListener(FutureListener listener) {
		this.channelFuture.addListener(listener);
	}

	public void removeFutureListener(FutureListener listener) {
		this.channelFuture.removeListener(listener);
	}

	public Session getSession() {
		return this.session;
	}

	public boolean isDone() {
		return this.channelFuture.isDone();
	}

	public boolean isCancelled() {
		return this.channelFuture.isCancelled();
	}

	public boolean isSuccess() {
		return this.channelFuture.isCancelled();
	}

	public Throwable getCause() {
		return this.channelFuture.cause();
	}

	public boolean cancel() {
		return this.channelFuture.cancel(false);
	}

	public ChannelFuture await() throws InterruptedException {
		return this.channelFuture.await();
	}

	public ChannelFuture awaitUninterruptibly() {
		return this.channelFuture.awaitUninterruptibly();
	}

	public boolean await(long timeout, TimeUnit unit)
			throws InterruptedException {
		return this.channelFuture.await(timeout, unit);
	}

	public boolean await(long timeoutMillis) throws InterruptedException {
		return this.channelFuture.await(timeoutMillis);
	}

	public boolean awaitUninterruptibly(long timeout, TimeUnit unit) {
		return this.channelFuture.awaitUninterruptibly(timeout, unit);
	}

	public boolean awaitUninterruptibly(long timeoutMillis) {
		return this.channelFuture.awaitUninterruptibly(timeoutMillis);
	}
}