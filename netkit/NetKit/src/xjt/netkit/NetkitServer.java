package xjt.netkit;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import org.apache.log4j.Logger;

import xjt.netkit.config.Config;
import xjt.netkit.http.HttpServer;

public class NetkitServer {
	private Logger logger = Logger.getLogger(NetkitServer.class);
	private final NetkitContext context;
	private String bindHost = "0.0.0.0";
	private int port = 8000;
	private int maxConnections = 10000;
	private ServerBootstrap bootstrap = null;
	private int maxMessagePacketSize = 1048576;
	private ScheduledExecutorService heatbeatService = Executors
			.newScheduledThreadPool(1);

	public int getMaxMessagePacketSize() {
		return this.maxMessagePacketSize;
	}

	public void setMaxMessagePacketSize(int maxPacketLength) {
		this.maxMessagePacketSize = maxPacketLength;
	}

	public NetkitServer(NetkitContext context) {
		this.context = context;
	}

	public NetkitServer(NetkitContext context, int port) {
		this.context = context;
		this.port = port;
	}

	public NetkitServer(NetkitContext context, String bindHost, int port) {
		this.context = context;
		this.port = port;
		this.bindHost = bindHost;
	}

	public void setMaxConnections(int maxConnections) {
		this.maxConnections = maxConnections;
	}

	public synchronized void startup() {
		NioEventLoopGroup bossExecutor = this.context.getBossExecutor();
		NioEventLoopGroup workerExecutor = this.context.getWorkerExecutor();
		this.context.setMaxConnections(this.maxConnections);
		NetkitChannelPipeline pipelineFactory = new NetkitChannelPipeline(
				this.context);
		bootstrap = new ServerBootstrap();
		bootstrap.group(bossExecutor, workerExecutor)
				.channel(NioServerSocketChannel.class)
				.childHandler(pipelineFactory)
				//.option(ChannelOption.SO_BACKLOG, 128)
				.childOption(ChannelOption.TCP_NODELAY, true)
				.childOption(ChannelOption.SO_KEEPALIVE, true);
		try {
			logger.info("NetkitServer is started in "+this.bindHost+":"+this.port+".");
			bootstrap.bind(this.bindHost, this.port).sync();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
/*		logger.info("Tcp server start heatbeatService.");
		this.heatbeatService.scheduleAtFixedRate(new HeatbeatListener(
				this.context), 30L, 30L, TimeUnit.SECONDS);*/
		HttpServer httpServer = new HttpServer(this.context);
		httpServer.start();
		logger.info("Http admin is started.Visit http://127.0.0.1:"+Config.HTTP_PORT);
	}

	public void shutdown() {
		try {
			this.context.release();
			this.heatbeatService.shutdown();
		} catch (Exception e) {
		}
	}

	public NetkitContext getServerContext() {
		return this.context;
	}
}