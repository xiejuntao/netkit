package xjt.netkit;

import org.apache.log4j.Logger;

import xjt.netkit.event.ChannelEvent;
import xjt.netkit.event.NetkitChannelEventListener;
import xjt.netkit.message.Message;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import io.netty.util.concurrent.Future;
import io.netty.util.concurrent.FutureListener;

public class NetkitChannelHandler extends SimpleChannelInboundHandler<Message> {
	private static final Logger logger = Logger
			.getLogger(NetkitChannelHandler.class);
	private NetkitContext context;
	private NetkitChannelEventListener channelEventListener;
	private Session session;

	public NetkitChannelHandler(NetkitContext context) {
		this.context = context;
		this.channelEventListener = context.getChannelEventListener();
	}

	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Message message)
			throws Exception {
		if (this.channelEventListener != null) {
			ChannelEvent evt = new ChannelEvent(this.context, ctx.channel());
			this.channelEventListener.onMessageReceived(evt, message);
		}
		logger.info("receive message content:"+message.getMessageContentAsString());
		this.session.onRecvMessage(message);
	}

	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		if (this.channelEventListener != null) {
			ChannelEvent evt = new ChannelEvent(this.context, ctx.channel());
			this.channelEventListener.onConnected(evt);
		}
		int sessionSize = this.context.getSessionSize();
		if (sessionSize > this.context.getMaxConnections()) {
			Message message = new Message(0);
			this.session.sendMessage(message).addFutureListener(
					new FutureListener<Object>() {
						@Override
						public void operationComplete(Future<Object> future)
								throws Exception {
							session.close();
						}
					});
			logger.warn("[WARNING]:The server connections has reached maximum.");
		}
		this.session = this.context.createSession(ctx.channel());
		super.channelActive(ctx);
	}

	@Override
	public void channelInactive(ChannelHandlerContext ctx) throws Exception {
		if (this.channelEventListener != null) {
			ChannelEvent evt = new ChannelEvent(this.context, ctx.channel());
			this.channelEventListener.onDisconnected(evt);
		}
		super.channelInactive(ctx);
	}

	@Override
	public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause)
			throws Exception {
		if (this.channelEventListener != null) {
			ChannelEvent evt = new ChannelEvent(this.context, ctx.channel());
			this.channelEventListener.onExceptionCaught(evt, cause);
		}
		super.exceptionCaught(ctx, cause);
	}
}
