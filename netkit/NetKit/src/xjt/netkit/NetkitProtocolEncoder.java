package xjt.netkit;

import xjt.netkit.message.Message;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;

public class NetkitProtocolEncoder extends MessageToByteEncoder<Message> {

	@Override
	protected void encode(ChannelHandlerContext ctx, Message message,
			ByteBuf out) throws Exception {
		if (message != null) {
			byte[] bytes = message.getMessageContent();
			out.writeInt(message.getMessageId());
			out.writeInt(message.getContentLength());
			if (bytes != null) {
				out.writeBytes(bytes);
			}
		}
	}
}
