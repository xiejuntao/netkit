package xjt.netkit;

import xjt.netkit.exception.ActionException;
import xjt.netkit.message.Message;

public interface ActionFilter {
	public void doFilter(Action paramAction, Message paramMessage, ActionChain paramActionChain)
		    throws ActionException;
}
