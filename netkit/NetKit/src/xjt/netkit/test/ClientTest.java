package xjt.netkit.test;

import org.apache.log4j.Logger;

import xjt.netkit.NetkitProtocolDecoder;
import xjt.netkit.NetkitProtocolEncoder;
import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;

public class ClientTest {
	static Logger logger = Logger.getLogger(ClientTest.class);
	public static void main(String[] args) {
			EventLoopGroup group = new NioEventLoopGroup();
			Bootstrap b = new Bootstrap();
			final ClientHandler clientHandler = new ClientHandler();
			b.group(group).channel(NioSocketChannel.class)
					.option(ChannelOption.TCP_NODELAY, true)
					.handler(new ChannelInitializer<SocketChannel>() {
						@Override
						public void initChannel(SocketChannel ch) throws Exception {
							ChannelPipeline pipeline = ch.pipeline();
							pipeline.addLast(new NetkitProtocolDecoder());
							pipeline.addLast(new NetkitProtocolEncoder());
							pipeline.addLast(clientHandler);
						}
					});
			try {
				b.connect("127.0.0.1",9001).sync();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
