package xjt.netkit.test;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;

import org.apache.log4j.Logger;

import xjt.netkit.message.Message;
import xjt.netkit.message.MessageUtil;

public class ClientHandler extends SimpleChannelInboundHandler<Message>{
	Logger logger = Logger.getLogger(ClientHandler.class);
	@Override
	public void channelActive(ChannelHandlerContext ctx) throws Exception {
		super.channelActive(ctx);
		ctx.writeAndFlush(MessageUtil.getMessage(MessageId.TEST,"天凉好个秋！"));
	}
	@Override
	protected void channelRead0(ChannelHandlerContext ctx, Message msg)
			throws Exception {
		logger.info("receive:"+msg.getMessageContentAsString());
		
	}
	
}
