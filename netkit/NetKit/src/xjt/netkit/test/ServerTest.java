package xjt.netkit.test;

import xjt.netkit.NetkitContext;
import xjt.netkit.NetkitServer;

public class ServerTest {

	public static void main(String[] args) {
		//创建Context
		NetkitContext netkitContext = new NetkitContext();
		//注册指定的消息处理Action
		netkitContext.registerAction(MessageId.TEST, TestAction.class);
		//创建指定端口的服务器
		NetkitServer netkitServer = new NetkitServer(netkitContext, 9001);
		//启动服务器
		netkitServer.startup();
	}
}
