package xjt.netkit.test;

import org.apache.log4j.Logger;

import xjt.netkit.Action;
import xjt.netkit.exception.ActionException;
import xjt.netkit.message.Message;
import xjt.netkit.message.MessageUtil;

public class TestAction extends Action {
	Logger logger = Logger.getLogger(TestAction.class);
	@Override
	public void execute(Message paramMessage) throws ActionException {
		logger.info("deal message:"+paramMessage.getMessageContentAsString());
		getSession().sendMessage(MessageUtil.getMessage(79,System.currentTimeMillis()+":"+"服务端反馈给客户端的消息"));
	}

}
