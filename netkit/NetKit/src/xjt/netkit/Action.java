package xjt.netkit;

import xjt.netkit.exception.ActionException;
import xjt.netkit.message.Message;

public abstract class Action {
	private Session session;

	public abstract void execute(Message paramMessage) throws ActionException;

	public Session getSession() {
		return this.session;
	}

	protected void setSession(Session session) {
		this.session = session;
	}
}
