此框架封装了netty4.x，提供统一的接口，自定义消息Id和处理消息的Action.并提供简单的http监控连接。
示例代码:
\
```
#!java

//创建Context
NetkitContext netkitContext = new NetkitContext();
//注册指定的消息处理Action
netkitContext.registerAction(MessageId.TEST, TestAction.class);
//创建指定端口的服务器
NetkitServer netkitServer = new NetkitServer(netkitContext, 9001);
//启动服务器
netkitServer.startup();
```

请直接运行服务端和客户端测试示例
xjt.netkit.test.ServerTest和xjt.netkit.test.ClientTest

项目有待进一步优化，交流学习QQ408760851。